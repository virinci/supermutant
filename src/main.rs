#[allow(dead_code)]
fn recur<const N: usize>(arr: &mut [u8; N], index: usize, remaining: u64) {
    if index == N {
        println!("{arr:?}");
        return;
    }

    for n in 0..N {
        let mask = 1_u64 << (N - n - 1);
        if remaining & mask != 0 {
            let save = arr[index];
            arr[index] = n as u8;
            recur(arr, index + 1, remaining & !mask);
            arr[index] = save;
        }
    }
}

#[allow(dead_code)]
fn natural_permutations<const N: usize>() {
    recur::<N>(&mut [0; N], 0, (1 << N as u64) - 1);
}

fn algorithm(n: usize) -> Vec<u8> {
    assert_ne!(n, 0, "n can't be 0");

    if n == 1 {
        return vec![1];
    }

    let previous = algorithm(n - 1);
    let mut current = vec![];

    let sum = n * (n - 1) / 2;
    let mut overlapping = n - 2;
    current.extend_from_slice(&previous[..overlapping]);

    for w in previous.windows(n - 1) {
        if w.iter().map(|x| *x as u64).sum::<u64>() != sum as u64 {
            overlapping = overlapping.saturating_sub(1);
            continue;
        }

        assert!(overlapping <= n - 2);
        current.extend_from_slice(&w[overlapping..]);
        current.push(n as u8);
        current.extend_from_slice(w);
        overlapping = n - 2;
    }

    current
}

fn main() {
    const N: usize = 4;
    // natural_permutations::<N>();

    let sp = algorithm(N);
    for w in sp.windows(N) {
        println!("{w:?}");
    }
}
